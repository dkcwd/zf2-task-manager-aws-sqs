<?php

require __DIR__ . '/../vendor/autoload.php';

$config = array(
    'modules' => array(
        'Aws',
        'SlmQueue',
        'SlmQueueSqs',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_static_paths' => array('example-config.php'),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);

Zend\Mvc\Application::init($config)->run();
