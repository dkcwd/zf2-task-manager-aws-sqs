#!/bin/sh

clear
## example of requesting a worker to poll the message queue
php endpoint.php queue sqs processQueue --waitTime=15 --visibilityTimeout=45