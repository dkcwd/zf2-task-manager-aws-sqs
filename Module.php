<?php

namespace Zf2TaskManagerAwsSqs;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        /**
         * You need to configure your AWS api key, secret and region values correctly to use SQS
         * Redefine the following array setting the correct values in your local application config
         *
         * 'aws' => array(
         *     'key'    => '',
         *     'secret' => '',
         *     'region' => ''
         * ),
         *
         */

        return array(
            'slm_queue' => array(
                'queue_manager' => array(
                    'factories' => array(
                        'processQueue' => 'SlmQueueSqs\Factory\SqsQueueFactory',
                    ),
                ),
                /**
                 * Redefine this part of the array in your local application
                 * config and provide the 'queue_url' value for your process queue
                 */
                'queues' => array(
                    'processQueue' => array(
                        'queue_url' => '',
                    ),
                ),
            ),
        );
    }
}
